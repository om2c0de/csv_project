import random

from django.core.management import BaseCommand

from customers.models import *


class Command(BaseCommand):
    @staticmethod
    def generate_phone_number():
        first_digit = random.randint(1, 9)
        remaining = random.randint(20000000, 30000000)
        return first_digit * 100 + remaining

    def handle(self, *args, **options):
        for i in range(10):
            customer = Customer.objects.create(
                first_name=f'first_name_{i}',
                last_name=f'last_name_{i}'
            )
            Email.objects.create(
                address=f'my_email_{i}@example.com',
                customer=customer
            )
            Phone.objects.create(
                number=self.generate_phone_number() + i,
                customer=customer
            )
