from rest_framework import serializers

from .models import Customer, Email, Phone


class EmailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Email
        fields = ['address']


class PhoneSerializer(serializers.ModelSerializer):
    class Meta:
        model = Phone
        fields = ['number']


class CustomerSerializer(serializers.ModelSerializer):
    emails = EmailSerializer(many=True, read_only=True)
    phones = PhoneSerializer(many=True, read_only=True)

    @staticmethod
    def setup_eager_loading(queryset):
        queryset = queryset.prefetch_related('emails', 'phones')
        return queryset

    class Meta:
        model = Customer
        fields = ['id', 'first_name', 'last_name', 'emails', 'phones']
