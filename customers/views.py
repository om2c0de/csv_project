from rest_framework import generics

from .models import Customer
from .serializers import CustomerSerializer


class CustomerListAPIView(generics.ListAPIView):
    serializer_class = CustomerSerializer

    def get_queryset(self):
        queryset = Customer.objects.only('first_name', 'last_name')
        queryset = self.get_serializer_class().setup_eager_loading(queryset)
        return queryset
