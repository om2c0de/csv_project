#!/usr/bin/env bash

docker-compose build --no-cache && docker-compose up && docker-compose run backend python manage.py makemigrations && docker-compose run backend python manage.py migrate
