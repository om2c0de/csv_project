FROM python:3.6
ENV PYTHONUNBUFFERED 1

# Set correct working directory
ENV WORKDIR /home/app/csv_project
RUN mkdir -p ${WORKDIR}
WORKDIR ${WORKDIR}

# Install python dependencies
COPY requirements.txt ${WORKDIR}
RUN pip install -r requirements.txt

# Run it
CMD gunicorn --bind 0.0.0.0:8000 csv_project.wsgi:application
